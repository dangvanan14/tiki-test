var server_url = '';

$(document).ready(function () {
    $.getJSON("./env.json", function (data) {
        server_url = data.server_url;

        fetch(server_url + "/product/getall")
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.success === true) {
                    if (json.data.length > 0) {
                        let childs = '<div class="col-lg-12"><h3>Danh sách sản phẩm</h3></div> ';
                        for (let i = 0; i < json.data.length; i++) {
                            let child = '<div class="col-lg-4">\n' +
                                '<div class="container-video">\n' +
                                '                                <img src="' + json.data[i].image + '"\n' +
                                '                                        frameborder="0" allowfullscreen class="video"></img>\n' +
                                '                            </div>' +
                                '                                    <h4>' + json.data[i].name + '</h4>\n' +
                                '                                    <p>' + json.data[i].price.toLocaleString() + ' USD</p>\n' +
                                '                                    <div class="heart white">\n' +
                                '                                        <p> <i style="cursor: pointer" class="fa fa-cart-plus" id="product_id_' + json.data[i].product_id + '" onclick="addToCart(this)"></i></p>\n' +
                                '                                    </div>\n' +
                                '                                </div>';
                            childs += child;
                        }
                        document.getElementById('list_product').innerHTML = childs;
                    }
                } else {

                }
            })
            .catch(function (ex) {
                console.error("parsing failed", ex);
            });
    });

    checkUser();
    var isLogin = true;

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $('#cart').click(function (e) {
        location.href = "./cart.html"
    });

    $('#login').click(function (e) {
        let data = {
            email: $('#email').val(),
            name: $('#name').val(),
            password: $('#password').val(),
        };

        if (data.email == '') {
            alert('Vui lòng nhập Email.');
            return;
        }

        if (data.name == '' && !isLogin) {
            alert('Vui lòng nhập tên.');
            return;
        }

        if (!validateEmail(data.email)) {
            alert('Vui lòng nhập đúng định dạng Email.');
            return;
        }

        if (data.password == '') {
            alert('Vui lòng nhập mật khẩu.');
            return;
        }

        if (!isLogin) {
            fetch(server_url + '/user/insert', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data)
            })
                .then(function (response) {
                    return response.json();
                })
                .then(function (json) {
                    if (json.success) {
                        Login(data);
                    } else {
                        alert("Email đã tồn tại!")
                    }
                    console.log(json);
                });
        } else {
            Login(data);
        }
    });

    function Login(data) {
        fetch( server_url + '/user/login', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(data)
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                console.log(json);
                if (json.success) {
                    $('#name_login').show();
                    $('#name_login').text(json.data.name);
                    $('#id01').hide();
                    $('#show-login').hide();
                    $('#show-singin').hide();
                    $('#logout').show();
                    $('#cart').show();
                    setCookie("email", json.data.email, 1);
                    setCookie("name", json.data.name, 1);
                    setCookie("user_id", json.data.user_id, 1);
                } else {
                    alert("Sai thông tin email hoặc mật khẩu.");
                }
            });
    }

    $('#show-login').click(function (e) {
        $('#id01').show();
        $('#name_block').hide();
        $('#login').text('Đăng Nhập');
        isLogin = true;
    });

    $('#show-singin').click(function (e) {
        $('#id01').show();
        $('#login').text('Đăng Ký');
        isLogin = false;
    });

    $('#logout').click(function (e) {
        setCookie('email', '', 1);
        setCookie('name', '', 1);
        setCookie('user_id', '', 1);
        location.reload();
    });
});

function addToCart(e) {
    let id = e.getAttribute("id").split("_");
    let product_id = id[2];

    let email = getCookie("email");
    if (email == "") {
        $('#show-login').click();
    } else {
        let data = {
            product_id: product_id,
            user_id: getCookie("user_id"),
            action: 0
        };

        fetch(server_url + "/cart/insert", {
            method: "POST",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(data)
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                if (json.success === true) {
                    alert("Thêm vào giỏ hàng thành công!");
                } else {
                    alert("Thêm vào giỏ hàng thất bại!");
                }
            })
            .catch(function (ex) {
                console.error("parsing failed", ex);
            });
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkUser() {
    let email = getCookie("email");
    if (email != "") {
        $('#name_login').show();
        $('#name_login').text(getCookie("name"));
        $('#show-login').hide();
        $('#show-singin').hide();
        $('#logout').show();
        $('#cart').show();
    }
}
